package page;

import element.HowItWorksElements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HowItWorksPage extends HowItWorksElements {


    private WebDriver driver;

    public HowItWorksPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void skipTutorialPages() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(startBtn));

        startBtn.click();
        waitForExperiencePageLoad();
        skipBtn.click();
    }

    public void waitForExperiencePageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(skipBtn));
    }
}
