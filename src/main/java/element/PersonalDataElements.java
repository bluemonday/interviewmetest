package element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalDataElements {

    @FindBy(name = "firstName")
    protected WebElement firstNameInput;

    @FindBy(name = "lastName")
    protected WebElement lastNameInput;

    @FindBy(css = "input[type=file]")
    protected WebElement addPictureBtn;

    @FindBy(xpath = "//button[@type='button']/div/span[.='Zapisz']")
    protected WebElement savePictureBtn;

    @FindBy(css = "div._1XYsq")
    protected WebElement namePreview;

    @FindBy(css = "img._25f67")
    protected WebElement picturePreview;

    private final WebDriver driver;

    public PersonalDataElements(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
