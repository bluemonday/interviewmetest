import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import page.PersonalDataPage;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class InterviewMeTest {

    private static WebDriver driver;

    @BeforeAll
    static void start() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.get("https://app.interviewme.pl/template/concept");
    }

    @Test
    void checkNameAndPicture() throws IOException {
        PersonalDataPage personalDataPage = new PersonalDataPage(driver);

        personalDataPage.setName();
        String namePreviewText = personalDataPage.getNamePreviewText();
        Assertions.assertEquals("Example Person", namePreviewText, "Name preview is invalid!");

        personalDataPage.setProfilePicture();
        BufferedImage profilePicturePreview = personalDataPage.downloadPreviewPicture();
        BufferedImage expectedPicture = ImageIO.read(
                new File("src/main/resources/profile_picture.png"));
        ImageDiffer imgDiff = new ImageDiffer();
        ImageDiff diff = imgDiff.makeDiff(profilePicturePreview, expectedPicture);
        Assertions.assertFalse(diff.hasDiff(), "Profile picture preview is invalid!");
    }

    @AfterAll
    static void close() {
        driver.quit();
    }
}
